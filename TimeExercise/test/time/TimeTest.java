package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetMilliSecondsRegular() {
		assertTrue("Result does not match expected", Time.getMilliSeconds("12:05:05:05") == 5);
	}

	@Test(expected = StringIndexOutOfBoundsException.class)
	public void testGetMilliSecondsException() {
		assertFalse("Result does not match expected", Time.getMilliSeconds("12:05:05") == 5);
	}

	@Test
	public void testGetMilliSecondsBoundaryIn() {
		assertTrue("Result does not match expected", Time.getMilliSeconds("12:05:05:00") == 0);
	}
	
	@Test
	public void testGetMilliSecondsBoundaryOut() {
		assertFalse("Result does not match expected", Time.getMilliSeconds("12:05:05:60") == 0);
	}
	
}
